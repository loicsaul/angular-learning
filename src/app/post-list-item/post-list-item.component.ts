import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() postTitle: string;
  @Input() postResume: string;
  @Input() postDateCreation: Date;
  loveItScore: number;

  constructor() { }

  ngOnInit() {
    this.loveItScore = 0;
  }

  onClickLoveIt(button) {
    this.loveItScore = button === 'love' ? this.loveItScore + 1 : this.loveItScore - 1;
  }
}
