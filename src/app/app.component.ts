import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Angular OC Learning';

  postsList = [
    {
      postTitle: 'Post 1',
      postResume: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tellus in urna ultricies malesuada id nec lectus.',
      postDateCreation: new Date()
    },
    {
      postTitle: 'Post 2',
      postResume: 'Vivamus ornare lorem sed neque tempus, in posuere nisi faucibus. Vestibulum id diam viverra, vehicula augue non',
      postDateCreation: new Date()
    },
    {
      postTitle: 'Post 3',
      postResume: 'Proin auctor quam est, a elementum purus porta in. Nullam et consequat augue. Sed malesuada leo quis diam condimentum.',
      postDateCreation: new Date()
    }
  ];
}
